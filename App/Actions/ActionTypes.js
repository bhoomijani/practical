export const MONTH_CHANGE = 'monthChange';
export const SET_DATETIME = 'setDatetime';
export const CALL_PENDING = 'callPending';
export const CALL_ERROR = 'callError';
export const CALL_SUCCESS = 'callSuccess';
