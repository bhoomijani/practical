import {connect} from 'react-redux';

import * as Actions from './ActionTypes';
import ConsumptionComponent from '../Components/ConsumptionComponent';
const moment = require('moment');

const mapStateToProps = state => ({
  loader: state.ConsumptionReducer.loader,
  error: state.ConsumptionReducer.error,
  data: state.ConsumptionReducer.data,
  totalKWH: state.ConsumptionReducer.totalKWH,
  totalKR: state.ConsumptionReducer.totalKR,
  errMessage: state.ConsumptionReducer.errMessage,
  currentTab: state.ConsumptionReducer.currentTab,
  currentDate: state.ConsumptionReducer.currentDate,
});

const mapDispatchToProps = dispatch => ({
  callService: state => dispatch(callWebApi(state)),
  changeTab: state => {
    dispatch(changeTabService(state));
  },
  changeMonth: (type, newDate) => {
    dispatch(changeMonthService(type, newDate));
  },
});
// call when change the month by left/right arrow
export const changeMonthService = (type, newDate) => {
  return dispatch => {
    var currentDate =
      type === 0
        ? moment(newDate).subtract(1, 'M')
        : moment(newDate).add(1, 'M');
    dispatch(callChangeMonth(currentDate));
    dispatch(callWebApi(currentDate));
  };
};
//Call this funcation when change the tab
export const changeTabService = value => {
  return dispatch => {
    dispatch(callTabfunction(value));
  };
};
//Call on web Api call on pending
export const serviceActionPending = () => ({
  type: Actions.CALL_PENDING,
});
//Call on web Api call throws error
export const serviceActionError = error => ({
  type: Actions.CALL_ERROR,
  error: error,
});
//Call on web Api call on sucess
export const serviceActionSuccess = (data, totalKWH, totalKR) => ({
  type: Actions.CALL_SUCCESS,
  data: data,
  totalKWH: totalKWH,
  totalKR: totalKR,
});

//Api Call
export const callWebApi = currentDate => {
  var currentDate = currentDate;
  var startDate = moment(currentDate)
    .startOf('month')
    .format('YYYY-MM-DD');

  var endDate = moment(currentDate)
    .endOf('month')
    .format('YYYY-MM-DD');
  var startTimeDiff = moment.duration('04:30:00');
  startDate = moment(startDate).add(startTimeDiff);
  endDate = moment(endDate).add(startTimeDiff);

  startDate = moment(startDate).toISOString();
  endDate = moment(endDate)
    .add(1, 'd')
    .toISOString();

  return dispatch => {
    dispatch(serviceActionPending());
    var formBody = JSON.stringify({
      jsonrpc: '2.0',
      id: '1',
      method:
        'co.getbarry.megatron.controller.ConsumptionFreemiumController.getDailyConsumptionWithPrice',
      params: [startDate, endDate, 'CET'],
    });
    return fetch('https://jsonrpc.getbarry.dk/json-rpc', {
      method: 'POST',
      body: formBody,
    })
      .then(response => response.json())
      .then(responseJson => {
        let totalKWH = 0;
        let totalKR = 0;
        responseJson.result.map((item, index) => {
          totalKR += item.priceIncludingVat;
          totalKWH += item.value;
        });
        dispatch(serviceActionSuccess(responseJson.result, totalKWH, totalKR));
      })
      .catch(error => {
        dispatch(serviceActionError(error));
      });
  };
};
//
export const callTabfunction = value => ({
  type: Actions.SET_DATETIME,
  currentTab: value,
});

export const callChangeMonth = value => ({
  type: Actions.MONTH_CHANGE,
  currentDate: value,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConsumptionComponent);
