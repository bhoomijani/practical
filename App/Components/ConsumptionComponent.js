import React, {Component, Fragment} from 'react';
import {
  SafeAreaView,
  StatusBar,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  Alert,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
var moment = require('moment');
import styles from './styles';
const kWh = 'kWh';
const kr = 'kr.';
const inActiveTabColor = '#32363D';
const kwhTabColor = '#FF4B9B';
const krTabColor = '#EF7B40';

class Consumption extends Component {
  componentDidMount() {
    this.props.callService(new Date());
  }
  //alert consumption detail by onPress function
  alertBox = item => {
    var message =
      this.props.currentTab === 0
        ? 'Your Consumption is ' +
          item.value +
          'kWh as on ' +
          moment(item.date).format('ll')
        : 'Your Consumption price is ' +
          item.value +
          'kr as on ' +
          moment(item.date).format('ll');
    Alert.alert('getBerry', message);
  };
  //Header Section of FlatList
  flatListHeader = () => {
    return (
      <View style={styles.flatListHeader}>
        <Text
          style={[
            styles.fWhValue,
            {color: this.props.currentTab === 0 ? kwhTabColor : krTabColor},
          ]}>
          {this.props.currentTab === 0
            ? this.props.totalKWH.toFixed(2)
            : this.props.totalKR.toFixed(2)}
          <Text style={styles.textType}>
            {this.props.currentTab === 0 ? kWh : kr}
          </Text>
        </Text>
      </View>
    );
  };
  //Display empty message while no records found from Api call
  renderEmptyView = () => {
    return (
      <View style={styles.emptyBarView}>
        <Text style={styles.emptyText}> {this.props.errMessage} </Text>
      </View>
    );
  };
  // border separater view
  separator = () => {
    return <View style={styles.itemBorder} />;
  };
  //Top Section of the screen, You can change month by arrow
  topComponent = () => {
    return (
      <View style={styles.topView}>
        <View style={styles.headerBox}>
          <Text style={styles.headerStyle}> Your Consumption </Text>
        </View>
        <View style={styles.monthPicker}>
          <TouchableOpacity
            onPress={() => this.props.changeMonth(0, this.props.currentDate)}
            style={styles.arrows}>
            <Image
              style={styles.arrowIcon}
              source={require('./../../assets/images/left.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <View style={styles.monthView}>
            <Text style={styles.headerStyle}>
              {moment(this.props.currentDate).format('MMMM YYYY')}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => this.props.changeMonth(1, this.props.currentDate)}
            style={styles.arrows}>
            <Image
              style={styles.arrowIcon}
              source={require('./../../assets/images/right.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.tabStyle}>
          <View style={styles.tabBlock}>
            <TouchableOpacity
              onPress={() => this.props.changeTab(0)}
              style={[
                styles.tabButton,
                {
                  backgroundColor:
                    this.props.currentTab === 1
                      ? inActiveTabColor
                      : kwhTabColor,
                },
              ]}>
              <Text style={styles.tabText}>{kWh}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.changeTab(1)}
              style={[
                styles.tabButton,
                {
                  backgroundColor:
                    this.props.currentTab === 0 ? inActiveTabColor : krTabColor,
                },
              ]}>
              <Text style={styles.tabText}>{kr} </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  //Render list of consumption data by date
  bottomComponent = () => {
    return (
      <View style={styles.bottomView}>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={this.props.data}
          refreshing={this.props.loader}
          ItemSeparatorComponent={this.separator}
          ListEmptyComponent={this.renderEmptyView}
          ListHeaderComponent={this.flatListHeader}
          renderItem={({item}) => (
            <View>
              <TouchableOpacity
                style={styles.itemBox}
                onPress={this.alertBox.bind(this, item)}>
                <View>
                  <Text style={styles.itemValue}>
                    {moment(item.date).format('DD/MM YYYY')}
                  </Text>
                </View>
                <View style={styles.innerItemBox}>
                  <Text style={styles.itemValue}>
                    {this.props.currentTab === 1
                      ? item.priceIncludingVat.toFixed(2)
                      : item.value.toFixed(2)}
                  </Text>
                  <Text style={styles.itemSign}>
                    {this.props.currentTab === 0 ? kWh : kr}
                  </Text>
                  <View style={styles.imageCenterBox}>
                    <Image
                      style={styles.arrowIcon}
                      resizeMode="contain"
                      source={require('./../../assets/images/right.png')}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
    );
  };
  render() {
    return (
      <Fragment>
        <StatusBar backgroundColor="#2C2F36" barStyle="light-content" />
        <SafeAreaView style={styles.statusBar} />
        <SafeAreaView style={styles.mainContainer}>
          {this.props.loader && (
            <View style={styles.loaderView}>
              <ActivityIndicator size="large" color="#FF4B9B" />
            </View>
          )}
          <View style={styles.wrapper}>
            {this.topComponent()}
            {this.bottomComponent()}
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
}
Consumption.propTypes = {
  currentTab: PropTypes.number.isRequired,
  currentDate: PropTypes.object.isRequired,
  totalKWH: PropTypes.number.isRequired,
  totalKR: PropTypes.number.isRequired,
  errMessage: PropTypes.string.isRequired,
};

export default Consumption;
