import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  mainContainer: {
    flex: 9,
    backgroundColor: '#2C2F36',
  },
  wrapper: {
    flex: 9,
  },
  topView: {
    flex: 2.5,
    elevation: 2,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowColor: '#2C2F36',
    shadowRadius: 3,
  },
  monthPicker: {
    borderWidth: 0,
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
  },
  bottomView: {
    borderWidth: 0,
    flex: 6.5,
  },
  arrows: {
    padding: 8,
    borderWidth: 0,
  },
  monthView: {
    justifyContent: 'center',

    alignItems: 'center',
    alignContent: 'center',
  },
  tabStyle: {
    padding: 10,
    justifyContent: 'center',
    alignContent: 'center',
    flexDirection: 'row',
  },
  tabBlock: {
    borderRadius: 25,
    flexDirection: 'row',
    backgroundColor: '#32363D',
  },
  tabButton: {
    borderRadius: 25,
    width: 137,
    paddingTop: 10,
    paddingBottom: 10,
  },
  tabText: {
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 16,

    color: '#fff',
    fontStyle: 'normal',
    textAlign: 'center',
  },
  flatListHeader: {
    padding: 5,
    paddingBottom: 15,

    justifyContent: 'center',
    alignContent: 'center',
    paddingTop: 15,
  },
  textType: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
    fontStyle: 'normal',
    fontWeight: 'bold',
    opacity: 0.6,
  },
  fWhValue: {
    fontSize: 32,
    textAlign: 'center',
    fontStyle: 'normal',
    fontWeight: 'bold',
    lineHeight: 38,
  },
  statusBar: {
    color: '#fff',
    backgroundColor: '#2C2F36',
  },
  arrowIcon: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
  },
  itemValue: {
    lineHeight: 17,
    fontStyle: 'normal',
    color: '#fff',
    fontSize: 14,
  },
  headerBox: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  headerStyle: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    fontStyle: 'normal',
    fontWeight: '600',
  },
  itemSign: {
    color: '#fff',
    fontSize: 10,
    lineHeight: 12,
    fontStyle: 'normal',
    textAlign: 'center',
    textAlignVertical: 'center',
    padding: 5,
  },
  itemBorder: {
    backgroundColor: '#fff',
    height: 1,
    padding: 0,
    opacity: 0.6,
  },
  emptyBarView: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    lineHeight: 17,
    fontWeight: '600',
    fontStyle: 'normal',
    textAlign: 'center',
    fontSize: 14,
    color: '#fff',
  },
  itemBox: {
    paddingBottom: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'center',
    borderWidth: 0,
    flex: 2,
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 17,
  },
  innerItemBox: {
    flexDirection: 'row',
  },
  imageCenterBox: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  loaderView: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
  },
});
export default styles;
