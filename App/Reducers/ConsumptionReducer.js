import * as Actions from '../Actions/ActionTypes';

const ConsumptionReducer = (
  state = {
    loader: false,
    error: undefined,
    data: [],
    currentTab: 0,
    currentDate: new Date(),
    totalKWH: 0,
    totalKR: 0,
    errMessage: 'No data available',
  },
  action,
) => {
  switch (action.type) {
    case Actions.CALL_PENDING:
      return Object.assign({}, state, {
        loader: true,
      });

    case Actions.CALL_ERROR:
      return Object.assign({}, state, {
        loader: false,
        error: action.error,
        totalKWH: 0,
        totalKR: 0,
      });
    case Actions.CALL_SUCCESS:
      return Object.assign({}, state, {
        loader: false,
        data: action.data,
        totalKWH: action.totalKWH,
        totalKR: action.totalKR,
      });
    case Actions.SET_DATETIME:
      return Object.assign({}, state, {
        currentTab: action.currentTab,
      });
    case Actions.MONTH_CHANGE:
      return Object.assign({}, state, {
        currentDate: action.currentDate,
      });

    default:
      return state;
  }
};

export default ConsumptionReducer;
