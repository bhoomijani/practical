
Explaination                                
----------------------------------------------------------------------------
 react-native and used packages.                                
----------------------------------------------------------------------------
React-native 0.61.5 
react-redux  7.1.3
prop-types   15.7.2
react        16.9.0,
redux        4.0.5
redux-thunk  2.3.0
moment       2.24.0


How to Run in Android/iOS                          
----------------------------------------------------------------------------
Goto you Project Directory in terminal
cd practical
npm install for install npm packages
run the command to start application, make sure you started the emulator

react-native start
react-native run-android
react-native run-ios  

Application will be run into the emulator.


How to Work                                  
----------------------------------------------------------------------------
Initally display current month of Cunsumption data. 
You can change the month by Left/right arrow (Month picker)
Display the data according to month.
You can change the tab to see the data for kWh/kr
If no data get from api response then display the "No Data available"



